# Semantic layer data preparation

This repository describes the post-processing of semantic layer data derived from
the Sphaera project database.

The post-processing includes time-sliceing the data for various possible historical
assumptions, as well as the preparation of the data for visualization in
the [Muxviz](http://muxviz.net/) multilayer visualization tool.

## Raw data

The raw data for edges and book information is published separatly as 

> Victoria Beyer, Nana Citron, Manon Gumpert, Gesa Funke, Irina Tautschnig, Malte Vogl, Christoph Sander, Florian Kräutli, Matteo Valleriani (2019). Sphaera -- Edge and Book data. DARIAH-DE. https://doi.org/10.20375/0000-000c-1f68-e

The notebooks are directly loading data from the published repository. 

## Interactive notebooks

An interactive version of the Notebooks can be accessed via the GESIS Binder:

For the post-processing of the raw edge data:

[![Binder](https://notebooks.gesis.org/binder/badge_logo.svg)](https://notebooks.gesis.org/binder/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2FMPIWG%2FDepartment-I%2Fsphaera%2Fsphaera-semantic-data.git/1986662abc72e1b9a64a508b4c26dae24b07a810?filepath=1_Create_semantic_layers.ipynb)
File: 1_Create_semantic_layers.ipynb

For the creation of Muxviz formated data and the time slices:

[![Binder](https://notebooks.gesis.org/binder/badge_logo.svg)](https://notebooks.gesis.org/binder/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2FMPIWG%2FDepartment-I%2Fsphaera%2Fsphaera-semantic-data.git/1986662abc72e1b9a64a508b4c26dae24b07a810?filepath=2_Time_and_influence_batching.ipynb)
File: 2_Time_and_influence_batching.ipynb


## Muxviz example image

The image shows the output for a multilayer visualization of the semantic data. All books in the full time period are shown. Edges are allowed to have a maximum age of aorund 90 years, i.e. the difference between the publication dates of two connected books must be less then 90 years.

![Muxviz Image for full time period](t175_2.png)
